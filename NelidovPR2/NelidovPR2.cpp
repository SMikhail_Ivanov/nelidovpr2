// NelidovPR2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

int MasCount = 0;

struct InternetShop {
	int ID;
	std::string SecondName;
	std::string FirstName;
	std::string Patronymic;
	int Price;
	float Discount;
	std::string Address;
};

InternetShop NewMas[100];

void MainMenu() {
	system("cls");
	cout << "Welcome to Internet Shop!" << endl;
	cout << "Press 1 to Add." << endl;
	cout << "Press 2 to View." << endl;
	cout << "Press 3 to Edit." << endl;
	cout << "Press 9 to Exit." << endl;
}

void Add(int MasCount) {
	int Int;
	string String;
	float Float;
	
	NewMas[MasCount].ID = MasCount;
	cout << "Second Name: "; cin >> String;
	NewMas[MasCount].SecondName = String;
	cout << "First Name: "; cin >> String;
	NewMas[MasCount].FirstName = String;
	cout << "Patronymic: "; cin >> String;
	NewMas[MasCount].Patronymic = String;
	cout << "Price: "; cin >> Int;
	NewMas[MasCount].Price = 1000;
	cout << "Discount: "; cin >> Float;
	NewMas[MasCount].Discount = 10;
	cout << "Address: "; cin >> String;
	NewMas[MasCount].Address = String;
}

void View() {
	for (int i = 1; i <= MasCount; i++) {
		cout << NewMas[i].ID  << " - ID " << endl;
		cout << NewMas[i].SecondName << " - Second Name " << endl;
		cout << NewMas[i].FirstName << " - First Name " << endl;
		cout << NewMas[i].Patronymic << " - Patronymic" << endl;
		cout << NewMas[i].Price << " - Price " << endl;
		cout << NewMas[i].Discount << " - Discount " << endl;
		cout << NewMas[i].Address << " - Address " << endl;
		cout << " " << endl;
	}
	getchar();
}

void Edit() {	
	int a = 0;

	View();
	cout << "Take a number of person" << endl;
	cin >> a;
	Add(a);
}

int Exit()
{
	return 0;
}

int main() {
	int a = 0;
	
	while (a != 9) {

		MainMenu();
		cin >> a;
				
		switch (a) {
		
		case 1:
			Add(MasCount + 1);
			MasCount++;
			break;
		
		case 2:
			View();
			getchar();
			break;
		case 3:
			Edit();
			getchar();
			break;
		case 9:
			Exit();
			break;
		}
	}
}
